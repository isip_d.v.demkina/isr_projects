import math
import numpy as biblio_numpy
program = -1
while program == -1:
    print("Меню программы:")
    print("1 - Калькулятор")
    print("2 - Подсчет в строке")
    print("3 - Матрица")
    print("0 - Выход")
    program = int(input(""))
    while program == 1:
        print("Калькулятор")
        print("1 - Сложение")
        print("2 - Вычитание")
        print("3 - Умножение")
        print("4 - Деление")
        print("5 - Деление нацело")
        print("6 - Остаток от деления")
        print("7 - Степень")
        print("8 - Квадратный корень")
        print("9 - Факториал")
        print("10 - Синус")
        print("11 - Косинус")
        print("12 - Площадь параллелепипеда")
        print("13 - Периметр параллелепипеда")
        print("0 - Выход")

        func = input("")
        function = int(func)
        if function != 0:
            print("Первое число =")
            one = input("")
            func_1 = int(one)
            if function == 1:
                print("Второе число =")
                two = input("")
                func_2 = int(two)
                print("Ответ: ")
                print((func_1+func_2))
            if function == 2:
                print("Второе число =")
                two = input("")
                func_2 = int(two)
                print("Ответ: ")
                print((func_1-func_2))
            if function == 3:
                print("Второе число =")
                two = input("")
                func_2 = int(two)
                print("Ответ: ")
                print( (func_1*func_2))
            if function == 4:
                print("Второе число =")
                two = input("")
                func_2 = int(two)
                if func_2==0 :
                    print("Ошибка!")
                else:
                    print("Ответ: ")
                    print((func_1/func_2))
            if function == 5:
                print("Второе число =")
                two = input("")
                func_2 = int(two)
                if func_2==0 :
                    print("Ошибка!")
                else:
                    print("Ответ: ")
                    print((func_1//func_2))
            if function == 6:
                print("Второе число =")
                two = input("")
                func_2 = int(two)
                if func_2==0 :
                    print("Ошибка!")
                else:
                    print("Ответ: ")
                    print( (func_1%func_2))
            if function == 7:
                print("Второе число =")
                two = input("")
                func_2 = int(two)
                print("Ответ: ")
                print( (func_1**func_2))
                
                
            if function == 8:
                print("Ответ: ")
                print(math.sqrt(func_1))
            if function == 9:
                print("Ответ: ")
                print( math.factorial(func_1))
            if function == 10:
                print("Ответ: ")
                print( math.sin(func_1))
            if function == 11:
                print("Ответ: ")
                print( math.cos(func_1))
                
            if function == 12:
                print("Второе число =")
                two = input("")
                func_2 = int(two)
                print("Третье число =")
                three = input("")
                func_3 = int(three)
                print("Ответ: ")
                print(2*(func_1*func_2+func_1*func_3+func_2*func_3))
            if function == 13:
                print("Второе число =")
                two = input("")
                func_2 = int(two)
                print("Третье число =")
                three = input("")
                func_3 = int(three)
                print("Ответ: ")
                print((4*func_1)+(4*func_2)+(4*func_3))
        if function == 0:
            program = -1

    if program == 2:
        print("Строка")
        stroka = input("")
        func_str = str(stroka)
        print("Итог")
        print("Пробелы:")
        print(func_str.count(' '))
        print("Запятые:")
        print(func_str.count(','))
        print("Символы:")
        print(func_str.count('') - 1)
        program = -1
        
   
    if program == 3:
        print("Количество столбцов")
        one = input("")
        stolb = int(one)
        print("Количество строк")
        two = input("")
        strok = int(two)
        print("Первое число матрицы")
        three = input("")
        one_num = int(three)
        print("Шаг матрицы")
        four = input("")
        shag = int(four)
        kol_num = stolb*strok
        num = one_num
        for number in range(kol_num):
            num=num+shag
        print("Ответ:")
        print(biblio_numpy.arange(one_num,num,shag).reshape(stolb, strok))
        program = -1

print('Программа завершена')